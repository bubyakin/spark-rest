# Spark Framework for REST service #

This project created just for training and shows by example how to implement RESTful web service based on Spark Framework

### Tech stack ###

* [Java 8](http://www.oracle.com/technetwork/java/javase/8-whats-new-2157071.html)
* [Spark Framework](http://sparkjava.com/)
* [Jackson](https://github.com/FasterXML/jackson/)
* [Lombok](https://projectlombok.org/)
* [Gradle](http://spring.io/guides/gs/gradle/)

### How to run? ###
Update dependencies and run as simple java app via main method
Then application will run at [localhost:4567](http://localhost:4567)

### How to use? ###
Send **POST** to [localhost:4567/posts](http://localhost:4567/posts)
With a body

```
#!json
{
"title": "First publication!",
"content": "Try spark ;)",
"categories": ["java", "spark", "lombok"]
}
```
And **GET** to [localhost:4567/posts](http://localhost:4567/posts)

**To Be Continued** :)